﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nancy.ModelBinding;
using Nancy.Validation;

namespace Video_Store_Web_Site.API
{
    public class MovieSearchRequest
    {
        public string MovieTitle { get; set; }
        public int Year { get; set; }
        public string PrimaryGenre { get; set; }
        public string Director { get; set; }
        public IEnumerable<Model.Genre> OtherGenres { get; set; }
    }

    public class MovieSearchResponse : MovieSearchRequest
    {
        public MovieSearchResponse(Model.Movie movie)
        {
            MovieTitle = movie.MovieTitle;
            Year = movie.Year;
            Director = movie.Director;
            OtherGenres = new List<Model.Genre>();

            //  If PrimaryGenre is set, then OtherGenres will also have a least one element in it
            if (movie.PrimaryGenre != null)
            {
                PrimaryGenre = movie.PrimaryGenre.Name;

                //  Skip 1 here for the extra genre being added by our mapping of MovieGenres and the sort order starting at 1, rather than 0
                OtherGenres = movie.Genres.Skip(1).Where(x => !x.Equals(movie.PrimaryGenre));
            }
        }
    }

    public class Movie : BaseModule
    {       
        public Movie() : base("movie")
        {
            Post["/search"] = _ =>
            {
                var searchParameters = this.Bind<MovieSearchRequest>();

                return new List<MovieSearchResponse>()
                {
                    new MovieSearchResponse(Hoosiers)
                };
            };
        }

        /// <summary>
        /// Set up some test Genres here for when we're not connected to the database.
        /// Students shouldn't have to do anything with this method.
        /// </summary>
        static Movie()
        {
            //  Match the behavior of the somewhat broken mapping of Genres in Movie
            Hoosiers.Genres.Add(null);
            Hoosiers.Genres.Add(Hoosiers.PrimaryGenre);           
            Hoosiers.Genres.Add(new Model.Genre() { Name = "Action" });
            Hoosiers.Genres.Add(new Model.Genre() { Name = "Romance" });
        }
    }
}