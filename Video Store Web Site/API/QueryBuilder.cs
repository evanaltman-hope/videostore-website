﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using NHibernate;

namespace Video_Store_Web_Site.API
{
    public class QueryBuilder
    {
        private char _alias;
        private ParameterInfo _parameters;
        private StringBuilder _builder;
        private string _and;

        public QueryBuilder(string table)
        {
            this._parameters = new ParameterInfo();
            _alias = table[0];

            _builder = new StringBuilder(64);
            _builder.Append("from ");
            _builder.Append(table);
            _builder.Append(" ");
            _builder.Append(_alias);

            _and = " where ";
        }

        public void CheckSearchParameter(object parameterValue, string propertyName, DataType dataType)
        {
            bool isSet = false;
            switch (dataType)
            {
                case DataType.String:
                    isSet = parameterValue != null && !parameterValue.Equals("");
                    break;
                case DataType.Integer:
                    isSet = (int) parameterValue != -1;
                    break;
                case DataType.DateTime:
                    isSet = parameterValue != null;
                    break;
                default:
                    throw new ArgumentOutOfRangeException("dataType", dataType, null);
            }

            if (isSet)
            {
                _builder.Append(_and);

                _builder.Append(_alias);
                _builder.Append(".");
                _builder.Append(propertyName);
                _builder.Append("= :");
                _builder.Append(propertyName);

                _parameters[propertyName] = new ParameterValue() {Type = dataType, Value = parameterValue};
                _and = " and ";
            }
        }

        public IQuery ToQuery(ISession session)
        {
            var query = session.CreateQuery(_builder.ToString());
            _parameters.SetParameters(query);
            return query;
        }
    }
}