﻿var app = angular.module("imdb");

app.controller('CustomerCtrl', function ($scope, Restangular) {

    function makeEmptyCustomer() {
        return {
            Name: {
                "Prefix": "",
                "First": "",
                "Middle": "",
                "Last": "",
                "Suffix": ""
            },
            StreetAddress: "",
            Phone: "",
            ZipCode: {},
            Username: "",
            Password: ""
        };
    }

    /*  Taken from http://stackoverflow.com/questions/1244094/converting-json-results-to-a-date  */
    $scope.parseJsonDate = function (jsonDateString) {
        if (jsonDateString) {
            return new Date(parseInt(jsonDateString.replace('/Date(', '')));
        } else {
            return "";
        }
    }

    $scope.getCustomerFormLabel = function() {
        if ($scope.mode === "view") {
            return "Add a new customer";
        } else {
            return "Customer Detail";
        }
    }

    $scope.showDetails = function(customer) {
        $scope.mode = "details";
        $scope.newCustomer = customer;
        Restangular.all("/customer/" + customer.Id + "/rentals").getList().then(
            function(rentals) {
                $scope.rentals = rentals;
            }
        );
    }

    $scope.mode = "view";

    $scope.emptyCustomer = makeEmptyCustomer();

    $scope.titles = ["Dr.", "Mr.", "Ms.", "Mrs.", "Sir"];

    $scope.newCustomer = $scope.emptyCustomer;

    Restangular.all("zipcode").getList().then(
        function(zipcodes) {
            $scope.zipcodes = zipcodes;
        }
    );

    Restangular.all("customer").getList().then(
        function (customers) {
            $scope.customers = _.map (customers, function(customer) {
                if (!customer.Id) {
                    customer.Id = customer.Name.Last === 'Cusack' ? 1 : 2;
                }
                return customer;
            });
        }
    );

    $scope.saveNewCustomer = function (newCustomer) {        
        if ($scope.mode === "view") {
            Restangular.all("customer").post(newCustomer).then(
                function(customer) {
                    $scope.customers.push(customer);
                    $scope.newCustomer = makeEmptyCustomer();
                }
            );
        } else {
            Restangular.one("customer", newCustomer.Id).save().then(
                function(customer) {
                    alert("Changes saved");
                }
            );
        }
    };

    $scope.cancelNewCustomer = function() {
        if ($scope.mode === "details") {
            $scope.mode = "view"
        }

        $scope.newCustomer = makeEmptyCustomer();
    }
});